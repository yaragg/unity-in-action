using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIController : MonoBehaviour
{
    [SerializeField] TMP_Text scoreLabel;
    [SerializeField] SettingsPopup settingsPopup;
    [SerializeField] AudioSettingsPopup audioPopup;

    private int score;

    private void OnEnable()
    {
        Messenger.AddListener(GameEvent.ENEMY_HIT, OnEnemyHit);
    }

    private void OnDisable()
    {
        Messenger.RemoveListener(GameEvent.ENEMY_HIT, OnEnemyHit);
    }

    // Start is called before the first frame update
    void Start()
    {
        score = 0;
        scoreLabel.text = score.ToString();

        settingsPopup.Close();
        audioPopup.Close();
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void OnOpenAudioSettings ()
    {
        audioPopup.Open();
    }

    public void OnOpenSettings ()
    {
        settingsPopup.Open();
    }

    public void OnPointerDown ()
    {
        Debug.Log("pointer down");
    }

    public void OnSubmitName (string name)
    {
        Debug.Log(name);
    }

    public void OnSpeedValue (float speed)
    {
        Messenger<float>.Broadcast(GameEvent.SPEED_CHANGED, speed);
        PlayerPrefs.SetFloat("speed", speed);
        Debug.Log($"Speed: {speed}");
    }

    public void OnEnemyHit ()
    {
        score += 1;
        scoreLabel.text = score.ToString();
    }
}
