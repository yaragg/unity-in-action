using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeatherController : MonoBehaviour
{
    [SerializeField] Material sky;
    [SerializeField] Light sun;

    private float fullIntensity;

    private void OnEnable()
    {
        Messenger.AddListener(GameEvent.WEATHER_UPDATED, OnWeatherUpdated);
    }

    private void OnDisable()
    {
        Messenger.RemoveListener(GameEvent.WEATHER_UPDATED, OnWeatherUpdated);
    }

    // Start is called before the first frame update
    void Start()
    {
        fullIntensity = sun.intensity;
        sky.SetFloat("_Blend", 0);
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void SetOvercast(float value)
    {
        sky.SetFloat("_Blend", value);
        sun.intensity = fullIntensity - (fullIntensity * value);
    }

    private void OnDestroy()
    {
        sky.SetFloat("_Blend", 0);
    }

    private void OnWeatherUpdated ()
    {
        SetOvercast(Managers.Weather.cloudValue);
    }
}
